# Nitro Examples

This repo has an example PBS script and Nitro task file which can be submitted to Flux.

1. Edit nitro.pbs, adding your Flux account and uniqname for email.
2. Submit the Nitro job with 

		$ qsub nitro.pbs

3. After the job starts (assuming the job was 1.nyx.arc-ts.umich.edu), monitor the job with: 

		$ nitrostat 1.nyx.arc-ts.umich.edu
